from secrets import token_urlsafe
from jose import JWTError, jwt
from datetime import datetime, timedelta
from fastapi import Depends, status, HTTPException
from fastapi.security import OAuth2PasswordBearer
from . import schema


oauth2_scheme =  OAuth2PasswordBearer(tokenUrl='login')

#SECRET_KEY
#Algorithm
#Experation_Time


SECRET_KEY = "skubfi4ewuhorinslmYTGKHBK978798HBMBM66fgtryugUbjbj655e6bj"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def create_access_token(data: dict):
    to_encode = data.copy() 

    expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return encoded_jwt


def verify_access_token(token:str, credential_exceptions):

    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        id: str = payload.get("user_id")

        if not id:
            raise credential_exceptions

        token_data = schema.TokenData(id=id)
    except JWTError:
        raise credential_exceptions

    return token_data


def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Could validate credentials", 
    headers={"WWW-Authenticate": "NBearer"})

    return verify_access_token(token, credential_exceptions=credentials_exception)