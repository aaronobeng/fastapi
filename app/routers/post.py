from typing import List
from fastapi import Body, FastAPI, Response, status, HTTPException, Depends, APIRouter
from .. import models, schema, oauth2
from .. database import get_db
from sqlalchemy.orm import Session


router = APIRouter(
    prefix="/posts",
    tags=["Post"]
)

@router.get("/", response_model=List[schema.PostResponse])
async def get_posts(db: Session = Depends(get_db)):
    # posts = cursor.execute("""SELECT * FROM posts """)
    # posts = cursor.fetchall()
    posts = db.query(models.Post).all()
    print(posts)
    return posts


@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schema.PostResponse)
async def create_posts(post: schema.PostCreate, db: Session=Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):
    # try:
    #     cursor.execute(""" INSERT into posts (title, content, published) VALUES (%s, %s, %s ) RETURNING * """, (post.title, post.content, post.published))
    #     new_post = cursor.fetchone()
    #     conn.commit()
    # except psycopg2.Error as error:
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"UNABLE TO INSERT TO DATABASE\n{error}")
    #    # raise ValueError(f"UNABLE TO INSERT TO DATABASE\n{error}") from None
    new_post = models.Post(**post.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    if not new_post:
         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"UNABLE TO INSERT TO DATABASE\n{error}")
    return new_post


@router.get("/{id}", status_code=status.HTTP_200_OK, response_model=schema.PostResponse)
async def get_post(id: int, db: Session= Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):

       
    # cursor.execute(""" SELECT * from posts WHERE id = (%s) """, (str(id)))
    # new_post = cursor.fetchone()
    post = db.query(models.Post).filter(models.Post.id == id).first()
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"cannot find post with id: {id}")
    return post


@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_post(id: int, db: Session = Depends(get_db) ,user_id: int = Depends(oauth2.get_current_user)):

    
    # cursor.execute(""" DELETE FROM posts WHERE id = %s RETURNING * """, (str(id),))
    # deleted_post = cursor.fetchone()
    # conn.commit()

    deleted_post = db.query(models.Post).filter(models.Post.id == id)
    if not deleted_post.first():
        print("Nothing here")
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"cannot delete post id: {id} it does not exist")

    deleted_post.delete(synchronize_session=False)
    db.commit()

    return Response(status_code=status.HTTP_204_NO_CONTENT)


@router.put("/{id}", response_model=schema.PostResponse)
async def update_post(id: int, post:schema.PostBase, db: Session=Depends(get_db), user_id: int = Depends(oauth2.get_current_user)):
    # cursor.execute(""" UPDATE posts SET title = %s, content = %s, published = %s WHERE id = %s RETURNING * """, 
    #                                     (post.title, post.content, post.published, str(id)))
    # updated_post = cursor.fetchone()
    # conn.commit()
    updated_post = db.query(models.Post).filter(models.Post.id == id)

    if updated_post.first() == None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"cannot find post with id: {id} to update")

    updated_post.update(post.dict(), synchronize_session=False)

    db.commit()

    return updated_post.first()