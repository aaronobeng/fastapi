from typing import Optional, List
from fastapi import Body, FastAPI, Response, status, HTTPException, Depends, APIRouter
from .. import models, schema, utils
from .. database import engine, get_db
from sqlalchemy.orm import Session


router = APIRouter(
    prefix="/users",
    tags=["Users"]
)

@router.post("/", status_code=status.HTTP_201_CREATED, response_model=schema.UserResponse)
async def create_user(user: schema.UserCreate, db: Session=Depends(get_db)):

    # Hash password

    user.password = utils.hashish(user.password)

    new_user = models.User(**user.dict())
    db.add(new_user)
    db.commit()
    db.refresh(new_user)

    return new_user


@router.get("/{id}", status_code=status.HTTP_200_OK, response_model=schema.UserResponse)
async def get_user(id: int, db: Session=Depends(get_db)):
    
    user = models.User()
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"cannot find user with an id: {id}")
    return user