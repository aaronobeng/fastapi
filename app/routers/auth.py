from mmap import ACCESS_COPY
from typing import List
from fastapi import Body, FastAPI, Response, status, HTTPException, Depends, APIRouter
from fastapi.security import OAuth2PasswordRequestForm
from .. import models, schema, utils, oauth2
from .. database import get_db
from sqlalchemy.orm import Session


router = APIRouter(
    tags=["Authentication"]
)


@router.post("/login", status_code=status.HTTP_200_OK, response_model=schema.Token)
async def user_login(user_credentials: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):

    user = db.query(models.User).filter(models.User.email == user_credentials.username).first()

    if not user:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=f"Invalid credentials")
    
    if not utils.verify(user_credentials.password, user.password):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=f"Invalid credentials")

    # create and return a token
    user_token = schema.Token

    access_token = oauth2.create_access_token(data={"user_id": user.id})

    user_token.access_token = access_token
    user_token.token_type = "bearer"

    return {"access_token": access_token, "token_type": "bearer"}
