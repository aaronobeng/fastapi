from enum import auto
from multiprocessing import synchronize
from typing import Optional, List
from fastapi import Body, FastAPI, Response, status, HTTPException, Depends
from pkg_resources import yield_lines
import psycopg2
from psycopg2.extras import RealDictCursor
from . import models, schema
from . database import engine, get_db
from sqlalchemy.orm import Session
from .routers import post, user, auth


models.Base.metadata.create_all(bind=engine)


app = FastAPI()


my_post = [{"title": "Title of post 1", "content": "content of post 1", "id": 1},{"title": "Favourite food", "content": "I love my fufu", "id": 2}]

while True:
    try:
        conn = psycopg2.connect(host='localhost', database='fastapi', user='postgres', password='root', cursor_factory=RealDictCursor)
        cursor  = conn.cursor()
        print("Database connection is success")
        break
    except psycopg2.Error as error:
        raise ValueError(f"UNABLE TO CONNECT TO DATABASE\n{error}") from None
        time.sleep(2)


def find_post(id: int):
    result = [x for x in my_post if x["id"] == int(id)]
    if len(result) > 0:
        return result
    else:
        return None 


def find_post_index(id: int):
    result = [count for count, value in enumerate(my_post) if value["id"] == int(id)]
    if len(result) > 0:
        return result[0]
    else:
        return -1


app.include_router(post.router)
app.include_router(user.router)
app.include_router(auth.router)

